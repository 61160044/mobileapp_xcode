//
//  SideBar.swift
//  Homework_61160044
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct SideBar: View {
    var body: some View {
        NavigationView{
            List{
                NavigationLink(destination: CourseView){
                    Label("Courses", systemImage: "book.closed")
                }
                Label("Tutorial", systemImage: "list.bullet.rectangle")
                Label("Livestreams", systemImage: "tv")
                Label("Certificates", systemImage: "mail.stack")
                Label("Search", systemImage: "magnifyingglass")

            }
            .listStyle(SidebarListStyle())
            .navigationTitle("Learn")
            
        }
    }
}

struct SideBar_Previews: PreviewProvider {
    static var previews: some View {
        SideBar()
    }
}
