//
//  SwiftUIView.swift
//  Homework_61160044
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
        HStack(alignment: .top) {
            Image(systemName: "applelogo")
                .renderingMode(.template)
                .frame(width: 40, height: 40)
                .imageScale(.large)
                .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.white/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.black/*@END_MENU_TOKEN@*/)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            
            VStack(alignment:.leading){
                HStack{
                    Text("Apple")
                        .font(.headline)
                        .fontWeight(.bold)
                        .multilineTextAlignment(.leading)
                    Image(systemName: "checkmark.seal.fill").renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).frame(width: 20, height:20, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    Text("@Apple")
                        .foregroundColor(Color.gray)
                }
                
                VStack(alignment:.leading){
                    Text("นำ IPhone ของคุณมาแลก แล้วอัพเกรดเป็น IPhone 12 mini ใหม่")
                    Image("iphone").resizable().aspectRatio(contentMode: .fit)
                    Text("ขอแนะนำ IPhone 12 mini")
                    Text("apple.com")
                        .foregroundColor(Color.gray)
                    HStack{
                        Image(systemName:"bubble.right")
                        Text("20")
                            .fontWeight(.regular)
                        Image(systemName: "repeat")
                            .padding(.leading, 30.0)
                        Text("1124")
                            .fontWeight(.regular)
                        Image(systemName: "heart")
                            .padding(.leading, 30.0)
                        Text("3580")
                            .fontWeight(.regular)
                        Image(systemName: "square.and.arrow.up")
                            .padding(.leading, 30.0)
                        
                    }
                    HStack{
                        Image(systemName: "hammer.fill").foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.gray/*@END_MENU_TOKEN@*/)
                        Text("Promoted")
                            .fontWeight(.thin)
                        
                    }
            
                }
                
                Spacer()
                
            }
    
            Spacer()
        }
        
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
}
