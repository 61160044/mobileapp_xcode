//
//  Homework_61160044App.swift
//  Homework_61160044
//
//  Created by student on 12/18/20.
//

import SwiftUI

@main
struct Homework_61160044App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
