//
//  ContentView.swift
//  Homework_61160044
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
            HStack{
                Image("Image").resizable().aspectRatio(contentMode: .fit).frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                VStack(alignment:.leading){
                    Text("Mango Smoothie")
                        .font(.title)
                        .fontWeight(.bold)
                        .multilineTextAlignment(.leading)
                    Text("Mango, Banana, Almond Milk")
                        .font(.body)
                        .fontWeight(.regular)
                        .multilineTextAlignment(.leading)
                    Text("140 calories")
                        .font(.footnote)
                        .foregroundColor(Color.gray)
                        .multilineTextAlignment(.leading)
                    HStack{
                        Image(systemName: "star.fill")
                            .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                            .frame(width: 20, height: 20)
                            .imageScale(.large).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        Image(systemName: "star.fill")
                            .renderingMode(.template)
                            .frame(width: 20, height: 20)
                            .imageScale(.large).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        Image(systemName: "star.fill")
                            .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                            .frame(width: 20, height: 20)
                            .imageScale(.large).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        Image(systemName: "star.fill")
                            .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                            .frame(width: 20, height: 20)
                            .imageScale(.large).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                        Image(systemName: "star.fill")
                            .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(/*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                            .frame(width: 20, height: 20)
                            .imageScale(.large)
                            .imageScale(.large)
                
                    }
                    
                }
                
            }
        
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
}
