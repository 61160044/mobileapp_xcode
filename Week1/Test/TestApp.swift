//
//  TestApp.swift
//  Test
//
//  Created by student on 12/4/20.
//

import SwiftUI

@main
struct TestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
