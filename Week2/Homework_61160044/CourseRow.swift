//
//  CourseRow.swift
//  Test
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct CourseRow: View {
    var body: some View {
        HStack {
            Image(systemName:"paperplane.circle.fill")
                .renderingMode(.template)
                .frame(width: 48.0, height: 48.0)
                .imageScale(.large)
                .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.black/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            VStack(spacing: 4.0) {
                Text("SwiftUI")
                    .font(.headline)
                    .foregroundColor(Color.black)
                    
                Text("Desciption")
                    .font(.footnote)
                    .foregroundColor(Color.blue)
            }
            Spacer()
        }
        .font(.system(size: 34 , weight:.light,design:.rounded))
        
        
    }
}

struct CourseRow_Previews: PreviewProvider {
    static var previews: some View {
        CourseRow()
    }
}
