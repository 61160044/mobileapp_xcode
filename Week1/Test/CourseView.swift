//
//  CourseView.swift
//  Test
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct CourseView: View {
    var body: some View {
        List(0..<100) { item in
            CourseRow()
        }
        .listStyle(InsetGroupedListStyle())
    }
}

struct CourseView_Previews: PreviewProvider {
    static var previews: some View {
        CourseView()
    }
}
